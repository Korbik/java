# Copyright 2014-2018 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

myexparam branch=${PN/open}u
exparam -v BRANCH branch

myexparam jdk_update=$(ever range 4)
exparam -v JDK_UPDATE jdk_update

myexparam jdk_build=$(ever range 5)
exparam -v JDK_BUILD jdk_build
JDK_BUILD=${JDK_BUILD/p}

SLOT=$(ever range 1-2)
JTREG_VER=4.1-b10

require java [ virtual_jdk_and_jre_deps=false ] java-jdk
export_exlib_phases pkg_setup src_unpack src_prepare src_configure src_compile src_test_expensive src_install

if ever at_least 9.0 ; then
    SUMMARY="OpenJDK $(ever major)"
else
    SUMMARY="OpenJDK $(ever range 2)"
fi
HOMEPAGE="http://openjdk.java.net"

# Since they're tightly coupled with OpenJDK itself these days, we keep the
# following bundled components and do NOT use potentially existing system-wide
# installations:
COMPONENTS=( corba hotspot jaxp jaxws jdk langtools nashorn )
ever at_least 10.0 && COMPONENTS=()
BASE_URI="http://hg.openjdk.java.net"

if ever at_least 9.0 ; then
    DOWNLOADS="
        ${BASE_URI}/${JDK_UPDATE}/${BRANCH}/archive/${BRANCH/$(ever major)u}-${PV/_p*}+${JDK_BUILD}.tar.bz2 -> ${PNV}.tar.bz2
        https://adopt-openjdk.ci.cloudbees.com/job/jtreg/lastStableBuild/artifact/jtreg${JTREG_VER}.tar.gz
    "
else
    DOWNLOADS="
        ${BASE_URI}/${BRANCH}/${BRANCH}/archive/${BRANCH}${JDK_UPDATE}-b${JDK_BUILD}.tar.bz2 -> ${PNV}.tar.bz2
        https://adopt-openjdk.ci.cloudbees.com/job/jtreg/lastStableBuild/artifact/jtreg${JTREG_VER}.tar.gz
    "
fi

if ever at_least 9.0 ; then
    for component in "${COMPONENTS[@]}"; do
        DOWNLOADS+="
            ${BASE_URI}/${JDK_UPDATE}/${BRANCH}/${component}/archive/${BRANCH/$(ever major)u}-${PV/_p*}+${JDK_BUILD}.tar.bz2 -> ${PNV}-${component}.tar.bz2
        "
    done
else
    for component in "${COMPONENTS[@]}"; do
        DOWNLOADS+="
            ${BASE_URI}/${BRANCH}/${BRANCH}/${component}/archive/${BRANCH}${JDK_UPDATE}-b${JDK_BUILD}.tar.bz2 -> ${PNV}-${component}.tar.bz2
        "
    done
fi

if ever at_least 10.0; then
    : # Do nothing, let the user install required bootstrap dependencies first
elif ever at_least 9.0 ; then
    DOWNLOADS+="
        platform:amd64? (
            http://dev.exherbo.org/~philantrop/distfiles/openjdk8-bin-${BOOTSTRAP_VERSION}-amd64.tar.xz
        )
        platform:x86? (
            http://dev.exherbo.org/~philantrop/distfiles/openjdk8-bin-${BOOTSTRAP_VERSION}-x86.tar.xz
        )
    "
else
    DOWNLOADS+="
        platform:amd64? (
            http://dev.exherbo.org/~philantrop/distfiles/icedtea7-bin-${BOOTSTRAP_VERSION}-amd64.tar.xz
        )
        platform:x86? (
            http://dev.exherbo.org/~philantrop/distfiles/icedtea7-bin-${BOOTSTRAP_VERSION}-x86.tar.xz
        )
    "
fi

BUGS_TO="philantrop@exherbo.org"

LICENCES="GPL-2"
MYOPTIONS="
    doc
    examples
    ( platform: amd64 x86 )
"

# The tests are extremely expensive. Details in openjdk_src_test_expensive below.
RESTRICT="test"

DEPENDENCIES="
    build:
        app-arch/unzip
        app-arch/zip
        x11-proto/xcb-proto
        x11-proto/xorgproto
    build+run:
        app-misc/ca-certificates[>=20170717-r1][ca-trust]
        media-libs/freetype:2
        media-libs/giflib:=
        net-print/cups
        sys-sound/alsa-lib
        x11-libs/libX11
        x11-libs/libxcb
        x11-libs/libXext
        x11-libs/libXrender
        x11-libs/libXt
        x11-libs/libXtst
    recommendation:
        x11-apps/xrandr [[ description = [ Required to run OpenGL applications ] ]]
    suggestion:
        dev-lang/icedtea-web [[ description = [ Java Web Start & Java browser plugin (obsolete) ] ]]
"

if ever at_least 9.0 ; then
    WORK=${WORKBASE}/${BRANCH}-${BRANCH/$(ever major)u}-${PV/_p*}+${JDK_BUILD}
else
    WORK=${WORKBASE}/${BRANCH}-${BRANCH}${JDK_UPDATE}-b${JDK_BUILD}
fi

openjdk_pkg_setup() {
    if ever at_least 12; then
        if ! has_version dev-lang/openjdk11:11.0 && ! has_version dev-lang/openjdk12:12.0 ; then
            if has_version dev-lang/openjdk-bin:12.0; then
                MUST_BOOTSTRAP=1
                einfo "Auto-bootstrapping ${PN}"
            else
                eerror "openjdk11, openjdk12 or openjdk-bin:12.0 is required to bootstrap ${PN}"
                die "missing bootstrap requirements"
            fi
        else
            unset MUST_BOOTSTRAP
        fi
    elif ever at_least 11; then
        if ! has_version dev-lang/openjdk10:10.0 && ! has_version dev-lang/openjdk11:11.0 ; then
            if has_version dev-lang/openjdk-bin:11.0; then
                MUST_BOOTSTRAP=1
                einfo "Auto-bootstrapping ${PN}"
            else
                eerror "openjdk10, openjdk11 or openjdk-bin:11.0 is required to bootstrap ${PN}"
                die "missing bootstrap requirements"
            fi
        else
            unset MUST_BOOTSTRAP
        fi
    elif ever at_least 10.0; then
        if ! has_version dev-lang/openjdk9:9.0 && ! has_version dev-lang/openjdk10:10.0 ; then
            eerror "openjdk9 or openjdk10 is required to bootstrap ${PN}"
            die "missing bootstrap requirements"
        else
            unset MUST_BOOTSTRAP
        fi
    elif ever at_least 9.0 ; then
        if ! has_version virtual/jdk:1.8 && ! has_version dev-lang/openjdk9:9.0 ; then
            MUST_BOOTSTRAP=1
            einfo "Auto-bootstrapping ${PN}"
        else
            unset MUST_BOOTSTRAP
        fi
    else
        if ! has_version dev-lang/icedtea8 && ! has_version dev-lang/openjdk8 ; then
            MUST_BOOTSTRAP=1
            einfo "Auto-bootstrapping ${PN}"
        else
            unset MUST_BOOTSTRAP
        fi
    fi
}

openjdk_src_unpack() {
    default

    if ever at_least 9.0 ; then
        for component in "${COMPONENTS[@]}"; do
            edo mv "${component}"-${BRANCH/$(ever major)u}-${PV/_p*}+${JDK_BUILD} "${WORK}"/"${component}"
        done
    else
        for component in "${COMPONENTS[@]}"; do
            edo mv "${component}"-${BRANCH}${JDK_UPDATE}-b${JDK_BUILD} "${WORK}"/"${component}"
        done
    fi

    if expecting_tests --expensive ; then
        edo mv jtreg "${WORK}"/
    fi

    if option platform:amd64 && [[ -n ${MUST_BOOTSTRAP} ]] ; then
        if ever at_least 11.0 ; then
            : # nothing to do, we use openjdk-bin
        elif ever at_least 9.0 ; then
            unpack openjdk8-bin-${BOOTSTRAP_VERSION}-amd64.tar.xz
        else
            unpack icedtea7-bin-${BOOTSTRAP_VERSION}-amd64.tar.xz
        fi
    elif option platform:x86 && [[ -n ${MUST_BOOTSTRAP} ]] ; then
        if ever at_least 9.0 ; then
            unpack openjdk8-bin-${BOOTSTRAP_VERSION}-x86.tar.xz
        else
            unpack icedtea7-bin-${BOOTSTRAP_VERSION}-x86.tar.xz
        fi
    fi
}

openjdk_src_prepare() {
    edo chmod +x configure

    if ever at_least 9.0; then
        edo sed -e "s:/jvm/:/${PN}/:g" -i make/Install.gmk
        edo sed -e "s:-Werror::g" -i make/common/SetupJavaCompilers.gmk

        if ever at_least 11; then
            : # Nothing more to patch
        elif ever at_least 10.0; then
            edo sed -e "s:-Werror::g" -i make/autoconf/generated-configure.sh
            edo sed -i -e "s:/jvm/:/${PN}/:g" make/Install.gmk
        else
            edo sed -e "s:-Werror::g" -i common/autoconf/flags.m4
        fi
    else
        edo sed -i -e "s:-Werror::g" jdk/make/Setup.gmk
    fi

    [[ -f hotspot/make/linux/makefiles/gcc.make ]] && \
        edo sed -i -e "s:-Werror::g" hotspot/make/linux/makefiles/gcc.make

    default
}

openjdk_src_configure() {
    local myconf=()

    # Unset the Java variables in order to make sure no existing JDK gets picked
    # up.
    unset _JAVA_OPTIONS JAVA_HOME CLASSPATH JAVAC JAVACMD

    myconf+=(
        --hates=enable-fast-install
        --hates=disable-dependency-tracking
        --hates=disable-silent-rules
        --enable-unlimited-crypto
        # "configure: error: It is not possible to disable the use of alsa. Remove the --without-alsa option."
        --with-alsa
        --with-cacerts-file=/etc/ssl/certs/ca-certificates.crt
        --with-conf-name=Exherbo
        # "configure: error: It is not possible to disable the use of cups. Remove the --without-cups option."
        --with-cups
        --with-debug-level=release
        --with-extra-cflags="${CFLAGS}"
        --with-extra-cxxflags="${CXXFLAGS}"
        # The build system absolutely hates LDFLAGS containing "," (comma) so let's
        # play it safe and clear it. (Should be "${LDFLAGS}", of course.)
        --with-extra-ldflags=""
        --with-freetype-include=/usr/$(exhost --target)/include/freetype2
        --with-freetype-lib=/usr/$(exhost --target)/lib
        --with-giflib=system
        --with-jobs=${EXJOBS:-2}
        --with-jvm-variants=server
        --with-stdc++lib=dynamic
        # The build system hates iceccd, too.
        --with-tools-dir=/usr/$(exhost --target)/bin
        --with-x
        --with-zlib=system
    )

    if ever at_least 12; then
        myconf+=(
            --enable-cds_archive
            --enable-manpages
            --without-jmh
        )
    else
        myconf+=(
            --with-build-number=b${JDK_BUILD}
            #--with-milestone=u${JDK_UPDATE}-b${JDK_BUILD}
            --with-milestone=Exherbo
            --with-update-version=u${JDK_UPDATE}
        )
    fi

    if ever at_least 11; then
        myconf+=(
            --openjdk-target=$(exhost --target)
            --hates=build
            --hates=host
            --with-freetype=system
        )
    else
        myconf+=(
            --with-freetype=/usr/$(exhost --target)
            --disable-freetype-bundling
        )
    fi

    if ever at_least 9.0 ; then
        myconf+=(
            --with-cups=/usr/$(exhost --target)
            --disable-warnings-as-errors
            --with-version-opt=""
            --with-version-patch="${PV/#*_p}"
            --with-version-pre=""
        )
        if ! ever at_least 12; then
            myconf+=(
                --with-version-major=$(ever major)
            )
        fi
        if ! ever at_least 11; then
            myconf+=(
                --with-version-minor=$(ever range 2)
                --with-version-security=$(ever range 3)
            )
        fi
    fi

    if [[ -n ${MUST_BOOTSTRAP} ]] ; then
        if ever at_least 12.0 ; then
            myconf+=( --with-boot-jdk="$(ls -Ad /opt/openjdk-bin-12*)" )
        elif ever at_least 11.0 ; then
            myconf+=( --with-boot-jdk="$(ls -Ad /opt/openjdk-bin-11*)" )
        elif ever at_least 9.0 ; then
            myconf+=( --with-boot-jdk=${WORKBASE}/openjdk8 )
        else
            myconf+=( --with-boot-jdk=${WORKBASE}/icedtea7-${BOOTSTRAP_VERSION} )
        fi
    else
        if ever at_least 12 && [[ -x /usr/host/lib/openjdk12/bin/java ]]; then
            myconf+=( --with-boot-jdk=/usr/host/lib/openjdk12 )
        elif ever at_least 11 && [[ -x /usr/host/lib/openjdk11/bin/java ]]; then
            myconf+=( --with-boot-jdk=/usr/host/lib/openjdk11 )
        elif ever at_least 10.0 && [[ -x /usr/host/lib/openjdk10/bin/java ]]; then
            myconf+=( --with-boot-jdk=/usr/host/lib/openjdk10 )
        elif ever at_least 9.0 && [[ -x /usr/host/lib/openjdk9/bin/java ]]; then
            myconf+=( --with-boot-jdk=/usr/host/lib/openjdk9 )
        elif [[ -x /usr/host/lib/openjdk8/bin/java ]]; then
            myconf+=( --with-boot-jdk=/usr/host/lib/openjdk8 )
        elif [[ -x /usr/host/lib/icedtea8/bin/java ]]; then
            myconf+=( --with-boot-jdk=/usr/host/lib/icedtea8 )
        else
            eerror "You seem to have either openjdk10, openjdk9, openjdk8 and/or icedtea8 installed"
            eerror "but neither is usable. Re-install either or all of them before trying"
            eerror "to install ${PN} again. If this error persists get support in #exherbo."
            die "He's dead, Jim."
        fi
    fi

    if expecting_tests --expensive ; then
        myconf+=( --with-jtreg="${WORK}"/jtreg )
    else
        myconf+=( --without-jtreg )
    fi

    # BUILD_* actually needs to be an absolute path or else it won't be recognised.
    BUILD_CC=/usr/host/bin/$(exhost --build)-cc \
    BUILD_CXX=/usr/host/bin/$(exhost --build)-c++ \
    BUILD_LD=/usr/host/bin/$(exhost --build)-cc \
        econf "${myconf[@]}"
}

openjdk_src_compile() {
    local myconf=()

    myconf=(
        CONF=Exherbo
        JOBS=${EXJOBS:-2}
        LOG=debug
    )

    if ever at_least 9.0 ; then
        esandbox disable
    fi

    # The build system hates the -j parameter.
    edo make "${myconf[@]}" all

    if ever at_least 9.0 ; then
        esandbox enable
    fi
}

openjdk_src_test_expensive() {
    local myconf=()

    # If someone wants to work on this: There are literally thousands of tests
    # which makes this EXTREMELY expensive in terms of CPU time, RAM, I/O load
    # and duration. Surprisingly, most of the tests actually succeed.
    #
    # Selected issues:
    # - A few hundred tests need a running X server.
    # - Some fixable sandbox violations due to DNS requests.
    # - Some missing prerequisites (e. g. properly prepared keystore)
    # - With a total of 40 GB RAM (physical and swap combined), the tests were
    #   OOM-killed...

    return

    myconf=(
        CONF=Exherbo
        JOBS=${EXJOBS:-2}
        LOG=debug
    )

    # The build system hates the -j parameter. The build system hates a lot of things.
    edo make "${myconf[@]}" test
}

openjdk_src_install() {
    local build_target="${WORK}"/build/Exherbo
    local certpath=

    # Use ${PN} in java_home rather than ${PNV} or ${PN}-${SLOT} since the
    # name itself is implicitly slotted (the 6 in icedtea6 is equivalent in
    # meaning to SLOT=1.6, since JDK 6 is Java 1.6) and because the build
    # system expects to find an existing installation in /usr/lib*/icedtea7.
    local java_home="/usr/$(exhost --target)/lib/${PN}"

    dodir "${java_home}"

    # There's no (working) install target so we have to enter the build dir and
    # install everything manually.
    # Actually, since 9.0 there *is* a working install target but it's daft and
    # it has no suport for DESTDIR or anything like it...
    edo cd "${build_target}"

    if ever at_least 9.0 ; then
        insinto "${java_home}"
        doins -r "${build_target}"/images/jdk/*

        if ! option examples ; then
            edo rm -rf "${IMAGE}/${java_home}"/demo
        fi

        edo chmod 755 "${IMAGE}/${java_home}"/bin/*

        certpath="${java_home}"/lib/security/cacerts
    else
        # Install some extra docs.
        if option doc ; then
            dodoc -r docs/*
        fi

        edo cd "${build_target}"/images/j2sdk-image

        # The minimal set of standard docs.
        dodoc ASSEMBLY_EXCEPTION THIRD_PARTY_README

        # Install the JDK/JRE itself.
        insinto "${java_home}"
        doins -r bin include jre lib man release

        # Install the demos and samples if requested.
        if option examples ; then
            doins -r demo
            doins -r sample
        fi

        edo chmod 755 "${IMAGE}/${java_home}"/{,jre/}bin/*

        certpath="${java_home}"/jre/lib/security/cacerts
    fi

    # Link the Java KeyStore file generated by ca-certificates
    edo rm "${IMAGE}/${certpath}"
    dosym /etc/pki/ca-trust/extracted/java/cacerts "${certpath}"

    if ! ever at_least 9.0 ; then
        linkify_redundant_jdk_bins
    fi

    do_jdk_alternatives
}

